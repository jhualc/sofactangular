import {Injectable} from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';


@Injectable()
export class DestinosApiClient {
	destinos: DestinoViaje[];
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
	constructor() {
		this.destinos= [];
	}
	
	add(d:DestinoViaje){
		 
	  console.log("En la consola: " + d.nombre);
	  this.destinos.push(d);

	}

	getAll(): DestinoViaje[]{
		return this.destinos;
	} 

	getById( id:  string): DestinoViaje{
		return null;
	}
	
	elegir(d:DestinoViaje){
		
		this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);
	  }

	subscribeOnChange(fn){
		this.current.subscribe(fn);
	}
	
} 